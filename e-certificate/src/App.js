import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import {Container, CssBaseline} from '@material-ui/core'
import Register from "./content/form/Register"
import SignIn from "./content/form/SignIn"
import Copyright from "./content/Copyright"
import Submit from "./content/form/submitDone"
import Dashboard from "./content/dashboard/dashboard"
import AdminDash from "./content/admin/adminDash"
import UserDetails from "./content/admin/userDetails"
import Program from "./content/admin/program"
import Program1 from "./content/program/program1"

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
        <Container maxWidth="xl">
          <CssBaseline/>
          <Route exact path="/">
            <SignIn/>
          </Route>
          <Route path="/registration">
            <Register/>
          </Route>
          <Route path="/done">
            <Submit/>
          </Route>
          <Route path="/dashboard">
            <Dashboard/>
          </Route>
          <Route path="/adminDashboard">
            <AdminDash/>
          </Route>
          <Route path="/userDetails">
            <UserDetails/>
          </Route>
          <Route path="/program">
            <Program/>
          </Route>
          <Route path="/program1">
            <Program1/>
          </Route>
          <Copyright/>
        </Container>
        </Switch>
      </Router>
    </div>
  )
}

export default App
