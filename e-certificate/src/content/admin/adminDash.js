import { Typography, IconButton, AppBar, Toolbar, makeStyles, Drawer, List, ListItemIcon, ListItemText, ListItem, Menu, MenuItem, Card, CardContent, Box, Grid } from "@material-ui/core";
import { AccountCircle } from '@material-ui/icons/'
import ListIcon from '@material-ui/icons/List'
import PersonIcon from '@material-ui/icons/Person'
import HomeIcon from '@material-ui/icons/Home'
import { useHistory, useLocation } from "react-router-dom"
import { useState } from 'react'
import MaterialTable from "material-table"


const drawerWidth = 240

const style = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    title: {
        flexGrow: 1
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0
    },
    drawerPaper: {
        width: drawerWidth
    },
    drawerContainer: {
        overflow: 'auto'
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3)
    },
    active: {
        background : '#f1f1f1'
    }
}))

const AdminDash = () => {

    const classes = style()
    const history = useHistory()
    const location = useLocation()
    const [open, setopen] = useState()

    const handleClick = (event) => {
        setopen(event.currentTarget)
    }
    const handleClose = () => {
        setopen()
    }

    const drawerItem = [
        {
            text: 'Home',
            icon: <HomeIcon/>,
            path: '/adminDashboard'
        },
        {
            text: 'User',
            icon: <PersonIcon/>,
            path: '/userDetails'
        },
        {
            text: 'Program',
            icon: <ListIcon/>,
            path: '/program'
        },
    ]

    const columns = [
        {title: 'IC', field: 'ic'},
        {title: 'Name', field: 'name'},
        {title: 'Address', field: 'address'},
        {title: 'Tel', field: 'tel'},
        {title: 'Email', field: 'email'},
        {title: 'Company Name', field: 'cName'},
        {title: 'Company Address', field: 'cAddress'}
    ]

    const data = [
        {ic: '960604-08-6553', name: 'Arif Ansar', tel: '017-5457499', address: 'Laman Mazmida', email: 'arep.epy@gmail.com', cName: ' Mazmida Catering', cAddress: 'Metia Residence' },
        {ic: '010130-08-0723', name: 'Alif Najmi', tel: '014-2345598', address: 'Taiping', email: 'alifnajmi3001@gmail.com' }
    ]

    return (
        <div className={classes.root}>
            <AppBar position='fixed' className={classes.appBar}>
                <Toolbar>
                    <Typography variant='h6' className={classes.title}>
                        Home
                    </Typography>
                    <IconButton edge='end' color='inherit' onClick={handleClick}>
                        <AccountCircle/>
                    </IconButton>
                    <Menu
                        anchorEl={open}
                        keepMounted
                        open={Boolean(open)}
                    >
                        <MenuItem onClick={handleClose}>Profile</MenuItem>
                        <MenuItem onClick={() => history.push('/')} >Sign Out</MenuItem>
                    </Menu>
                </Toolbar>
            </AppBar>
            <Drawer variant='permanent' className={classes.drawer} classes={{paper: classes.drawerPaper}}>
                <Toolbar/>
                <div className={classes.drawerContainer}>
                    <List>
                        {drawerItem.map((item) => (
                            <ListItem 
                                button 
                                key={item.text} 
                                onClick={() => history.push(item.path)}
                                className={ location.pathname === item.path ? classes.active : null }
                            >
                                <ListItemIcon>{item.icon}</ListItemIcon>
                                <ListItemText primary={item.text} />
                            </ListItem>
                        ))}
                    </List>
                </div>
            </Drawer>
            <main className={classes.content}>
                <Toolbar/>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Card>
                            <CardContent>
                                <Typography variant="h6" align="center" gutterBottom>Total Program</Typography>
                                <Typography variant="h1" align="center">027</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={4}>
                        <Card>
                            <CardContent>
                                <Typography variant="h6" align="center" gutterBottom>Active Program</Typography>
                                <Typography variant="body1" align="center" gutterBottom>Program Makanan Mampu Milik</Typography>
                                <Typography variant="subtitle1" align="left">Date: 05/05/21 - 10/05/21</Typography>
                                <Typography variant="subtitle1" align="left">Total Participant: 25</Typography>
                                <Typography variant="subtitle1" align="left">Status: On Going</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={4}>
                        <Card>
                            <CardContent>
                                <Typography variant="h6" align="center" gutterBottom>Total Participant</Typography>
                                <Typography variant="h1" align="center">186</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Box p={1}/>
                <div style={{maxWidth: '100%'}}>
                        <MaterialTable
                        title='New Entry :'
                        columns={columns}
                        data={data}
                    />
                </div>
            </main>
        </div>
    );
}
 
export default AdminDash;