import { Typography, Button, IconButton, AppBar, Toolbar, makeStyles, Drawer, List, ListItemIcon, ListItemText, ListItem, Menu, MenuItem, Box } from "@material-ui/core";
import { AccountCircle } from '@material-ui/icons/'
import ListIcon from '@material-ui/icons/List'
import PersonIcon from '@material-ui/icons/Person'
import HomeIcon from '@material-ui/icons/Home'
import AddIcon from '@material-ui/icons/Add'
import DeleteIcon from '@material-ui/icons/Delete'
import { Dialog, DialogTitle, DialogContent, DialogActions, Grid, TextField } from "@material-ui/core";
import { useHistory, useLocation } from "react-router-dom"
import { useState } from 'react'
import EditIcon from '@material-ui/icons/Edit'
import MaterialTable from "material-table";

const drawerWidth = 240

const style = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    title: {
        flexGrow: 1
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0
    },
    drawerPaper: {
        width: drawerWidth
    },
    drawerContainer: {
        overflow: 'auto'
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3)
    },
    active: {
        background : '#f1f1f1'
    }
}))

const Program = () => {

    const classes = style()
    const history = useHistory()
    const location = useLocation()
    const [open, setopen] = useState()
    const [Open, setOpen] = useState()

    const handleClick = (event) => {
        setopen(event.currentTarget)
    }

    const handleOpen = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

    const drawerItem = [
        {
            text: 'Home',
            icon: <HomeIcon/>,
            path: '/adminDashboard'
        },
        {
            text: 'User',
            icon: <PersonIcon/>,
            path: '/userDetails'
        },
        {
            text: 'Program',
            icon: <ListIcon/>,
            path: '/program'
        },
    ]

    const columns = [
        {title: 'Code', field: 'code'},
        {title: 'Title', field: 'title'},
        {title: 'Program description', field: 'description'},
        {title: 'Tel', field: 'tel'},
        {title: 'PIC', field: 'pic'},
        {title: 'Date', field: 'date'},
    ]

    const data = [
        {code: '1001', title: 'Halal Awareness', tel: '017-5457499', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit magna nibh, a congue nunc dignissim non.', pic: 'admin', date: ' 12/06/21' },
        {code: '1003', title: 'Program Makanan Mampu Milik', tel: '014-2345598', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit magna nibh, a congue nunc dignissim non.', pic: 'admin', date: '05/05/21' }
    ]

    const action = [
        { icon: EditIcon, tooltip: 'Edit', onClick: handleOpen },
        { icon: DeleteIcon, tooltip: 'Delete', onClick: (event, rowData) => alert(rowData.name + ' has been removed. ') }
    ]

    return (
        <div className={classes.root}>
            <AppBar position='fixed' className={classes.appBar}>
                <Toolbar>
                    <Typography variant='h6' className={classes.title}>
                        Program Information
                    </Typography>
                    <IconButton edge='end' color='inherit' onClick={handleClick}>
                        <AccountCircle/>
                    </IconButton>
                    <Menu
                        anchorEl={open}
                        keepMounted
                        open={Boolean(open)}
                    >
                        <MenuItem onClick={handleClose}>Profile</MenuItem>
                        <MenuItem onClick={() => history.push('/')} >Sign Out</MenuItem>
                    </Menu>
                </Toolbar>
            </AppBar>
            <Drawer variant='permanent' className={classes.drawer} classes={{paper: classes.drawerPaper}}>
                <Toolbar/>
                <div className={classes.drawerContainer}>
                    <List>
                        {drawerItem.map((item) => (
                            <ListItem 
                                button 
                                key={item.text} 
                                onClick={() => history.push(item.path)}
                                className={ location.pathname === item.path ? classes.active : null }
                            >
                                <ListItemIcon>{item.icon}</ListItemIcon>
                                <ListItemText primary={item.text} />
                            </ListItem>
                        ))}
                    </List>
                </div>
            </Drawer>
            <main className={classes.content}>
                <Toolbar/>
                <Button variant="outlined" endIcon={<AddIcon/>} onClick={handleOpen}>
                    Add
                </Button>
                <Box p={1}/>
                <Dialog open={Open} onClose={handleClose}>
                    <DialogTitle onClose={handleClose}>
                        Add new program :
                    </DialogTitle>
                    <DialogContent dividers>
                        <Grid container spacing={2}>
                            <Grid item xs={8}>
                                <TextField
                                    label="Title : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    label="Code : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="Program description : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                    multiline
                                    rows={3}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label=" Date and Time :"
                                    type="datetime-local"
                                    InputLabelProps={{
                                        shrink: true
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="subtitle1">Person in charge details :</Typography>
                                <TextField
                                    label="Name : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label="Phone No : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleClose} color="primary">
                            Save Changes
                        </Button>
                    </DialogActions>
                </Dialog>
                <div style={{maxWidth: '100%'}}>
                        <MaterialTable
                        title='Program List :'
                        columns={columns}
                        data={data}
                        actions={action}
                        options={
                            {actionsColumnIndex: -1, exportButton: true}
                        }
                    />
                </div>
            </main>
        </div>
    );
}
 
export default Program;