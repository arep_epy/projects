import { Typography, IconButton, Button, AppBar, Toolbar, makeStyles, Drawer, List, ListItemIcon, ListItemText, ListItem, Menu, MenuItem, Box, Dialog, DialogTitle, DialogContent, DialogActions, Grid, TextField } from "@material-ui/core";
import { AccountCircle } from '@material-ui/icons/'
import AddIcon from '@material-ui/icons/Add'
import ListIcon from '@material-ui/icons/List'
import PersonIcon from '@material-ui/icons/Person'
import HomeIcon from '@material-ui/icons/Home'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import { useHistory, useLocation } from "react-router-dom"
import { useState } from 'react'
import firebase from '../../firebase'
import MaterialTable from "material-table";

const drawerWidth = 240

const style = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    title: {
        flexGrow: 1
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0
    },
    drawerPaper: {
        width: drawerWidth
    },
    drawerContainer: {
        overflow: 'auto'
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3)
    },
    active: {
        background : '#f1f1f1'
    }
}))

const UserDetails = () => {

    const classes = style()
    const history = useHistory()
    const location = useLocation()
    const [open, setopen] = useState()
    const [Open, setOpen] = useState()

    const handleClick = (event) => {
        setopen(event.currentTarget)
    }
    const handleClose = () => {
        setOpen(false)
    }
    const handleOpen = () => {
        setOpen(true)
    }

    const drawerItem = [
        {
            text: 'Home',
            icon: <HomeIcon/>,
            path: '/adminDashboard'
        },
        {
            text: 'User',
            icon: <PersonIcon/>,
            path: '/userDetails'
        },
        {
            text: 'Program',
            icon: <ListIcon/>,
            path: '/program'
        },
    ]

    const db = firebase.firestore()

    db.collection('User').get().then((snapshot) => {
        snapshot.docs.forEach(doc => {
            console.log(doc.data())
        })
    })

    const columns = [
            {title: 'IC', field: 'ic'},
            {title: 'Name', field: 'name'},
            {title: 'Address', field: 'address'},
            {title: 'Tel', field: 'tel'},
            {title: 'Email', field: 'email'},
            {title: 'Company Name', field: 'cName'},
            {title: 'Company Address', field: 'cAddress'}
    ]

    const data = [
        {ic: '960604-08-6553', name: 'Arif Ansar', tel: '017-5457499', address: 'Laman Mazmida', email: 'arep.epy@gmail.com', cName: ' Mazmida Catering', cAddress: 'Metia Residence' },
        {ic: '010130-08-0723', name: 'Alif Najmi', tel: '014-2345598', address: 'Taiping', email: 'alifnajmi3001@gmail.com' }
    ]

    const action = [
        { icon: EditIcon, tooltip: 'Edit', onClick: handleOpen },
        { icon: DeleteIcon, tooltip: 'Delete', onClick: (event, rowData) => alert(rowData.name + ' has been removed. ') }
    ]

    return (
        <div className={classes.root}>
            <AppBar position='fixed' className={classes.appBar}>
                <Toolbar>
                    <Typography variant='h6' className={classes.title}>
                        User Information
                    </Typography>
                    <IconButton edge='end' color='inherit' onClick={handleClick}>
                        <AccountCircle/>
                    </IconButton>
                    <Menu
                        anchorEl={open}
                        keepMounted
                        open={Boolean(open)}
                    >
                        <MenuItem onClick={handleClose}>Profile</MenuItem>
                        <MenuItem onClick={() => history.push('/')} >Sign Out</MenuItem>
                    </Menu>
                </Toolbar>
            </AppBar>
            <Drawer variant='permanent' className={classes.drawer} classes={{paper: classes.drawerPaper}}>
                <Toolbar/>
                <div className={classes.drawerContainer}>
                    <List>
                        {drawerItem.map((item) => (
                            <ListItem 
                                button 
                                key={item.text} 
                                onClick={() => history.push(item.path)}
                                className={ location.pathname === item.path ? classes.active : null }
                            >
                                <ListItemIcon>{item.icon}</ListItemIcon>
                                <ListItemText primary={item.text} />
                            </ListItem>
                        ))}
                    </List>
                </div>
            </Drawer>
            <main className={classes.content}>
                <Toolbar/>
                <Typography gutterBottom/>
                <Dialog open={Open} onClose={handleClose}>
                    <DialogTitle onClose={handleClose}>
                        Edit user :
                    </DialogTitle>
                    <DialogContent dividers>
                        <Grid container spacing={2}>
                            <Grid item xs={8}>
                                <TextField
                                    label="Name : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    label="IC Number : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="Address : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                    multiline
                                    rows={3}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="Email : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label="Phone No : "
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    fullWidth
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleClose} color="primary">
                            Save Changes
                        </Button>
                    </DialogActions>
                </Dialog>
                <Box p={1}/>
                <div style={{maxWidth: '100%'}}>
                        <MaterialTable
                        title='User Details :'
                        columns={columns}
                        data={data}
                        actions={action}
                        options={
                            {actionsColumnIndex: -1, exportButton: true}
                        }
                    />
                </div>
            </main>
        </div>
    );
}
export default UserDetails;