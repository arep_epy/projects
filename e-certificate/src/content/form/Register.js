import { Box, FormLabel, Tab, Tabs, Typography, Link, Container } from '@material-ui/core'
import { useState } from 'react'
import logo from '../../img/logo.png'
import Personal from '../plans/Personal'
import Business from '../plans/Business'

const Register = () => {

    const [value, setValue] = useState ()

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    return ( 
        <Container maxWidth="md">
        <Box bgcolor="#fff" boxShadow={2} mt={5} p={2} marginBottom={2}>
            <Box display="flex" justifyContent="center" p={1}>
                <Link className="logo" href="https://www.halal-academy.com/" alt="logo link">
                    <img className="logo" src={logo} alt="logo"/>
                </Link>
            </Box>
            <Box p={4}>
                <Typography variant="h5" align="center">Registration</Typography>
            </Box>
            <div>
                <Box mt={2}/>
                <FormLabel>Please select your plans :</FormLabel>
                <Box p={1}/>
                <Box bgcolor="#b9e1ac" borderRadius={5}>
                    <Tabs centered variant="fullWidth" textColor="primary" value={value} onChange={handleChange}>
                        <Tab label="Personal"/>
                        <Tab label="Business"/>
                    </Tabs>
                </Box>
                {value === 0 && <Personal type="personal"/>}
                {value === 1 && <Business type="business"/>}
            </div>
        </Box>
        </Container>
     );
}
 
export default Register;