import { useFormik } from 'formik'
import * as yup from 'yup'
import { Box, Button, Container, Link, TextField, Typography } from '@material-ui/core'
import logo from '../../img/logo.png'

const validationSchema = yup.object().shape({

    IC: yup
        .string()
        .length(14, 'Enter correct IC')
        .matches(/[0-9]+/gi, "Invalid IC no.")
        .required()
        .typeError("IC is invalid")
})

const SignIn = () => {
    
    const formik = useFormik({
        
        initialValues: {
            IC: ""
        },

        onSubmit: (values) => {
            console.log(JSON.stringify(values))
            window.location.replace('/dashboard')
            
        },
        validationSchema: validationSchema,
    })

    return (
        <Container maxWidth="sm">
        <Box bgcolor="#fff" boxShadow={2} mt={20} p={2} marginBottom={2}>
            <Box display="flex" justifyContent="center" p={1}>
                <Link className="logo" href="https://www.halal-academy.com/" alt="logo link">
                    <img className="logo" src={logo} alt="logo"/>
                </Link>
            </Box>
            <Box p={4}>
                <Typography variant="h5" align="center">Halal-Academy E-Certificate System</Typography>
            </Box>
            <Box pt={4}>
                <Typography variant="subtitle1" color="textPrimary">Please Insert your Identification Number :</Typography>
            </Box>
            <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
                <Box pt={2}>
                    <TextField
                        id = "IC"
                        label="IC. No."
                        fullWidth
                        onChange={formik.handleChange}
                        variant="outlined"
                        color="primary"
                        value={formik.values.IC}
                        error={formik.touched.IC && Boolean(formik.errors.IC)}
                        helperText={formik.touched.IC && formik.errors.IC}
                        onBlur={formik.handleBlur}
                    />
                </Box>
                <Box pt={2}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large" >
                        Submit
                    </Button>
                </Box>
            </form>
        </Box>
        </Container>
    )
}
 
export default SignIn;