import { Box, Typography, Link, Container } from '@material-ui/core'
import logo from '../../img/logo.png'

const Submit = () => {
    return (
        <Container maxWidth="sm">
        <Box bgcolor="#fff" boxShadow={2} mt={5} p={2} marginBottom={2}>
            <Box display="flex" justifyContent="center" p={1}>
                <Link className="logo" href="https://www.halal-academy.com/" alt="logo link">
                    <img className="logo" src={logo} alt="logo"/>
                </Link>
            </Box>
            <Box p={4}>
                <Typography variant="subtitle1" align="center">Thank you for submitting. We'll be contact you shortly.</Typography>
            </Box>
        </Box>
        </Container>
    );
}
 
export default Submit;