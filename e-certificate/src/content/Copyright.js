import { Typography, Link, Box } from "@material-ui/core";

const Copyright = () => {
    return (
        <Box p={4}>
        <Typography variant="body2" color="textSecondary" align="center" gutterBottom>
            { 'Copyright © '}
            <Link color="error" href="https://www.halal-academy.com/">Halal-Academy</Link>
            {' '}
        {new Date().getFullYear()}
        {' . '}
        All Right Reserved .
        </Typography>
        </Box>
    );
}
 
export default Copyright