import { Box, Link, Container, Button, Typography } from '@material-ui/core'
import React from 'react'
import logo from '../../img/logo.png'

const Program1 = () => {

    const info = ({
        title : "Program Makanan Mampu Milik",
        date : "16 March 2021",
        time : " 10.00am - 6.00pm",
        priceC : "Industry : RM 120 / pax",
        priceS : "Student/Alumni : RM 90 / pax",
        payInfo1 : "Online Transfer, CDM or Cheque(Claimable)",
        payInfo2 : "Refund can be made for cancelation atleast a week before",
        bank : "MAYBANK",
        accNo : "5122 2261 2682",
        accName : "GAE REASOURCES SDN BHD",
        pic1 : "Fathihah",
        no1 : "010-9294976",
        pic2 : " Puan Jannah",
        no2 : "011-12561926"

    })

    const handleChange = () => {
        window.location.replace('/registration')
    }

    return (
        <Container maxWidth="md">
        <Box bgcolor="#fff" boxShadow={2} mt={5} p={2} marginBottom={2}>
            <Box display="flex" justifyContent="center" p={1}>
                <Link className="logo" href="https://www.halal-academy.com/" alt="logo link">
                    <img className="logo" src={logo} alt="logo"/>
                </Link>
            </Box>
            <Box p={4}>
                <Typography align="center" variant="h5">{info.title}</Typography>
                <Box p={0.5}/>
                <Typography align="center" variant="body2">{info.date}{info.time}</Typography>
                <Box p={0.5}/>
                <Typography align="center" color="primary">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</Typography>
                <Box p={1}/>
                <Typography variant="h6">Price</Typography>
                <Typography variant="subtitle1">- {info.priceC}</Typography>
                <Typography variant="subtitle1">- {info.priceS}</Typography>
                <Box p={1}/>
                <Typography variant="h6">Payment</Typography>
                <Typography variant="subtitle1">- *{info.payInfo1}</Typography>
                <Typography variant="subtitle1">- *{info.payInfo2}</Typography>
                <Typography variant="body1">- {info.bank}</Typography>
                <Typography variant="body1">- {info.accNo}</Typography>
                <Typography variant="body1">- {info.accName}</Typography>
                <Box p={1}/>
                <Typography variant="h6" display="block">Person In Charge :</Typography>
                <Typography variant="subtitle1">{info.pic1} : {info.no1}</Typography>
                <Typography variant="subtitle1">{info.pic2} : {info.no2}</Typography>
            </Box>
            <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                size="large"
                onClick={handleChange} >
                Register
            </Button>
        </Box>
        </Container>
    );
}
 
export default Program1;