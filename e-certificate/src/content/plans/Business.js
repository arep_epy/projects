import { Box, Button, Grid, Snackbar, TextField, Typography } from "@material-ui/core";
import { useFormik } from 'formik'
import * as yup from 'yup'
import { useState } from 'react'
import firebase from '../../firebase'

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const validationSchema = yup.object().shape({
    Cname:
        yup
        .string()
        .required("This field is required"),
    Fname: 
        yup
        .string()
        .required("This field is required"),
    IC:    
        yup
        .string()
        .length(14, 'Enter correct IC')
        .matches(/[0-9]+/gi, "Invalid IC no.")
        .required()
        .typeError("IC is invalid"),
    Caddress:
        yup
        .string()
        .required("This field is required"),
    Address: 
        yup
        .string()
        .required("This field is required"),
    Tel:
        yup
        .string()
        .matches(phoneRegExp, 'Phone number is not valid')
        .required("This field is required")
        .typeError("Phone No. is invalid"),
    Email:
        yup
        .string()
        .email("Invalid email")
        .required()
})

const Business = (props) => {
    
    const db = firebase.firestore()
    const store = firebase.storage()
    const [file, setfile] = useState(0)
    const [open, setOpen] = useState(false)

    const metadata = {
        contentType: 'application/pdf'
    }

    const handleChange = (e) => {
        setfile(e.target.files[0])
    }

    const handleLoad = () => {
        setOpen(true)
    }

    const formik = useFormik({
        initialValues: {
            Cname: "",
            Fname: "",
            IC: "",
            Caddress: "",
            Address: "",
            Tel: "",
            Email: "",
        },
        onSubmit: (values) => {

            var uploadTask = store.ref(`Payment/${file.name}`).put(file, metadata);

            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    // console.log('Upload is' + progress + '% done')
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED:
                            // console.log('Upload is paused')
                            break
                        case firebase.storage.TaskState.RUNNING:
                            // console.log('Upload is running')
                            break
                    }
                },
                (error) => {
                    switch (error.code) {
                        case 'storage/unknown':
                            break
                        case 'storage/canceled':
                            break
                    }
                },
                () => {
                    uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                        // console.log('File available at', downloadURL)

                        db.collection('User').add({
                            Cname: values.Cname,
                            Name: values.Fname,
                            IC: values.IC,
                            Class: props.type,
                            Caddress: values.Caddress,
                            Address: values.Address,
                            Tel: values.Tel,
                            Email: values.Email,
                            Payment: {
                                fileName: file.name,
                                fileUrl: downloadURL
                            }
                        })
                    })
                }
            )
            setTimeout(function(){
                window.location.replace("/done")
            }, 3000)
        },
        validationSchema: validationSchema
    })

    return (
        <Box mt={4}>
            <Typography variant="h5" color="textS" align="center" gutterBottom>Sign Up</Typography>
            <Box p={2}/>
            <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
                <Grid container spacing={4}>
                <Grid item md={12}>
                        <TextField 
                            id="Cname" 
                            label="Company Name :" 
                            variant="outlined" 
                            color="primary" 
                            fullWidth 
                            onChange={formik.handleChange}
                            value={formik.values.Cname}
                            error={formik.touched.Cname && Boolean(formik.errors.Cname)}
                            helperText={formik.touched.Cname && formik.errors.Cname}
                            onBlur={formik.handleBlur}
                        />
                    </Grid>
                    <Grid item md={12}>
                        <TextField 
                            id="Fname" 
                            label="Full Name :" 
                            variant="outlined" 
                            color="primary" 
                            fullWidth 
                            onChange={formik.handleChange}
                            value={formik.values.Fname}
                            error={formik.touched.Fname && Boolean(formik.errors.Fname)}
                            helperText={formik.touched.Fname && formik.errors.Fname}
                            onBlur={formik.handleBlur}
                        />
                    </Grid>
                    <Grid item md={6}>
                        <TextField 
                            id="IC" 
                            label="IC. No. :"
                            variant="outlined" 
                            color="primary" 
                            fullWidth 
                            onChange={formik.handleChange}
                            value={formik.values.IC}
                            error={formik.touched.IC && Boolean(formik.errors.IC)}
                            helperText={formik.touched.IC && formik.errors.IC}
                            onBlur={formik.handleBlur}
                        />
                    </Grid>
                    <Grid item md={12}>
                        <TextField 
                            id="Caddress" 
                            label="Company Address :" 
                            variant="outlined" 
                            color="primary" 
                            fullWidth 
                            multiline 
                            rows={4} 
                            onChange={formik.handleChange}
                            value={formik.values.Caddress}
                            error={formik.touched.Caddress && Boolean(formik.errors.Caddress)}
                            helperText={formik.touched.Caddress && formik.errors.Caddress}
                            onBlur={formik.handleBlur}
                        />
                    </Grid>
                    <Grid item md={12}>
                        <TextField 
                            id="Address" 
                            label="Address :" 
                            variant="outlined" 
                            color="primary" 
                            fullWidth 
                            multiline 
                            rows={4} 
                            onChange={formik.handleChange}
                            value={formik.values.Address}
                            error={formik.touched.Address && Boolean(formik.errors.Address)}
                            helperText={formik.touched.Address && formik.errors.Address}
                            onBlur={formik.handleBlur}
                        />
                    </Grid>
                    <Grid item md={6}>
                        <TextField 
                            id="Tel" 
                            label="Telephone No. :" 
                            variant="outlined" 
                            color="primary" 
                            fullWidth 
                            onChange={formik.handleChange}
                            value={formik.values.Tel}
                            error={formik.touched.Tel && Boolean(formik.errors.Tel)}
                            helperText={formik.touched.Tel && formik.errors.Tel}
                            onBlur={formik.handleBlur}
                        />
                    </Grid>
                    <Grid item md={6}>
                        <TextField 
                            id="Email" 
                            label="Email Address :" 
                            variant="outlined" 
                            color="primary" 
                            fullWidth 
                            onChange={formik.handleChange}
                            value={formik.values.Email}
                            error={formik.touched.Email && Boolean(formik.errors.Email)}
                            helperText={formik.touched.Email && formik.errors.Email}
                            onBlur={formik.handleBlur}
                        />
                    </Grid>
                    <Grid item md={6}>
                        <Typography 
                            variant="subtitle1" 
                            color="textSecondary" 
                            gutterBottom
                        >
                                Payment Detail :
                        </Typography>
                        <input 
                            id="Payment" 
                            type="file"
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item md={12}>
                        <Button 
                            type="submit" 
                            variant="contained" 
                            color="primary" 
                            fullWidth 
                            size="large"
                            onClick={handleLoad}
                            
                        >
                            Sign Up
                        </Button>
                        <Snackbar
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center'
                            }}
                            open={open}
                            message="Form Submitted!"
                            color="primary"
                        />
                    </Grid>
                    <Box mb={2}/>
                </Grid>
            </form>
        </Box>
    );
}
 
export default Business;