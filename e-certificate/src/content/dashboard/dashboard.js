import { Box, Typography, Link, TextField, Container, Button } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab"
import logo from '../../img/logo.png'
import { useState } from 'react'

const courseList = [
    { name: 'Halal Awareness', value: 1},
    { name: 'Halal Executive', value: 2},
    { name: 'Halal Executive KPT', value: 3},
    { name: 'Halal Executive MEDEC', value: 4}
]


const Dashboard = () => {

    const handleChange = () => {
        window.location.replace('/program1')
    }

    return (
        <Container maxWidth="md">
        <Box bgcolor="#fff" boxShadow={2} mt={5} p={2} marginBottom={2}>
            <Box display="flex" justifyContent="center" p={1}>
                <Link className="logo" href="https://www.halal-academy.com/" alt="logo link">
                    <img className="logo" src={logo} alt="logo"/>
                </Link>
            </Box>
            <Box p={12}>
                <Typography variant="h6" align="left">Welcome, User!</Typography>
                <Typography variant="subtitle1" align="left">Select your Course :</Typography>
                <Box pt={2}/>
                <Autocomplete 
                    options={courseList}
                    getOptionLabel={(option) => option.name}
                    renderInput={( params ) => <TextField {...params} label="Course List" variant="outlined"/>}
                />
                <Box pt={2}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large"
                        onClick={handleChange} >
                        View
                    </Button>
                </Box>
            </Box>
        </Box>
        </Container>
    );
}
 
export default Dashboard;